# Cytus II Song Data Converter

Converts Cytus II song file structure to a more human-usable format.

## Prerequisites

1. [Python3](https://www.python.org/downloads/).
2. [Cytus II](https://play.google.com/store/apps/details?id=com.rayark.cytus2) on an Android device.

## How to use

1. Copy the main .obb game file from your mobile device (from `Android/obb/com.rayark.cytus2/`). The `main` file has the song data, the `patch` file has mostly some video files.

2. Unpack the obb file.

3. Locate the `assets/AssetBundles` directory in the unpacked archive and use [AssetRipper](https://github.com/AssetRipper/AssetRipper) to extract assets from the `.ab` files.

- Some song data is also stored in `Android/data/com.rayark.cytus2/files/AssetBundles`. You can use [AssetRipper](https://github.com/AssetRipper/AssetRipper) to extract those as well.

4. After extracting, you can run the Python script in a terminal:
```
python3 convert_songdata.py <pathToTheExtractedFiles> <outputPath>
```

## Updating metadata

If you change the `chart_metadata.txt` file, and want to re-add the metadata to the converted songs, you can use the `add_metadata.py` script to do so, without reconverting all of the file structure.

```
python3 update_metadata.py <outputPath>
```
