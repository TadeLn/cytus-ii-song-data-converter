import os
import sys
import json
import yaml
import shutil
import hashlib

import __metadata_reader

extra_chart_metadata = __metadata_reader.load_metadata(os.path.join(os.path.dirname(__file__), "chart_metadata.txt"))
if extra_chart_metadata is None:
    extra_chart_metadata = {}



if len(sys.argv) < 3:
    print("\033[31merror: not enough arguments\033[0m", file=sys.stderr)
    print(f"\033[34musage: {sys.argv[0]} <input directory> <output directory> [source label]\033[0m", file=sys.stderr)
    exit(1)

input_directory = sys.argv[1]
output_directory = sys.argv[2]
source_label = None
if len(sys.argv) > 3:
    source_label = sys.argv[3]



songdata_directory = input_directory

for subdir in ["Assets", "Asset_Bundles", "songdata"]:
    new_path = os.path.join(songdata_directory, subdir)
    if os.path.exists(new_path):
        songdata_directory = new_path

print(f"\033[34msongdata directory: {songdata_directory}\033[0m")



extra_info_path = os.path.join(output_directory, "extra_info.json")
extra_info = {};
if os.path.exists(extra_info_path):
    with open(extra_info_path, "r") as extra_info_file:
        print("\nloading extra_info.json...")
        extra_info = json.load(extra_info_file)
else:
    extra_info = {
        "errors": [],
        "extra_paths": [],
        "missing_paths": [],
        "collisions": [],
        "metadata": {}
    }



# Remove path from extra_info["extra_paths"]
def use_path(path):
    if path in extra_info["extra_paths"]:
        extra_info["extra_paths"].remove(path)



# Traverse to path and call use_path() on every path segment on the way
def traverse_and_use(starting_path, path_segments):
    current_path = starting_path

    for subdir in path_segments:
        new_path = os.path.join(current_path, subdir)
        if os.path.exists(new_path):
            current_path = new_path
            use_path(current_path)
        else:
            # Specified subdirectory doesn't exist - ill-formated songdata
            print(f"\n\033[31m  error: traverse_and_use(): specified subdirectory {new_path} doesn't exist\033[0m")
            extra_info["missing_paths"].append(new_path)

    return current_path



# Use file and file.meta + save .meta data to extra_info
# Returns (True, path) if file exists, (False, None) if not
def use_file(file_path, required=False):
    exists = True
    use_path(file_path)

    if not os.path.exists(file_path):
        exists = False;
        if required:
            print(f"\n\033[31m  error: use_file(): file {file_path} doesn't exist\033[0m")
            extra_info["missing_paths"].append(file_path)
            return (False, None)

    meta_file_path = file_path + ".meta"
    use_path(meta_file_path)
    if os.path.exists(meta_file_path):
        with open(meta_file_path) as meta_file:
            try:
                metadata = yaml.safe_load(meta_file)   
                extra_info["metadata"][file_path] = metadata
            except yaml.YAMLError as ex:
                msg = f"use_file(): parsing yaml .meta file failed: {ex}"
                extra_info["errors"].append(msg)
                print(f"\n\033[31m  error: {msg}\033[0m")

    elif required:
        print(f"\n\033[31m  error: use_file(): .meta file for file {file_path} doesn't exist\033[0m")
        extra_info["missing_paths"].append(meta_file_path)

    return (exists, file_path if exists else None)



# Try to copy file from source to destination
# Returns True if source file exists
def try_copy(path, destination_path):
    (exists, source_path) = use_file(path, required=False)
    if exists:
        already_existed = os.path.exists(destination_path)
        if already_existed:
            # Compare hashes - if they don't match, record to extra_info
            try:
                source_hash = None
                with open(source_path, "rb") as source_file:
                    source_hash = hashlib.md5(source_file.read()).hexdigest()

                destination_hash = None
                with open(destination_path, "rb") as destination_file:
                    destination_hash = hashlib.md5(destination_file.read()).hexdigest()

                if source_hash != destination_hash:
                    msg = f"try_copy(): collision found - hashes don't match:\n  {source_hash} {source_path}\n  {destination_hash} {destination_path}"
                    extra_info["collisions"].append({
                        "source_path": source_path,
                        "source_hash": source_hash,
                        "destination_path": destination_path,
                        "destination_hash": destination_hash,
                    })
                    print(f"\n\033[31m  error: {msg}\033[0m")
                    print(f"\033[33m  the file has not been copied\033[0m")

            except Exception as ex:
                msg = f"try_copy(): comparing file hashes failed: {ex}"
                extra_info["errors"].append(msg)
                print(f"\n\033[31m  error: {msg}\033[0m")
        else:
            # Try copying if it didn't exist already
            try:
                shutil.copy2(source_path, destination_path)
            except Exception as ex:
                msg = f"try_copy(): copying file failed: {ex}"
                extra_info["errors"].append(msg)
                print(f"\n\033[31m  error: {msg}\033[0m")
    return exists



def shorten_string(string, start_length, end_length):
    threshold = start_length + end_length + 3
    if len(string) > threshold:
        return string[:start_length] + "..." + string[-end_length:]
    else:
        return string



# Record all files and directories in extra_paths, recursively
i = 0
for current_dir, subdirs, files in os.walk(songdata_directory):

    # Directories
    for dirname in subdirs:
        path = os.path.join(current_dir, dirname)
        extra_info["extra_paths"].append(path)
        print(f"\033[2K\033[1000D\033[37mscanning directory recursively... {i} - {shorten_string(path, 40, 50)}\033[0m", end="", flush=True)
        i += 1

    # Files
    for filename in files:
        path = os.path.join(current_dir, filename)
        extra_info["extra_paths"].append(path)
        print(f"\033[2K\033[1000D\033[37mscanning directory recursively... {i} - {shorten_string(path, 40, 50)}\033[0m", end="", flush=True)
        i += 1

print()


# Go through all songs and copy the chart and music files
for song_directory in os.scandir(songdata_directory):
    song_id = song_directory.name
    print(f"\033[2K\033[1000D\033[37mprocessing {song_id}...\033[0m", end="", flush=True)

    output_song_path = os.path.join(output_directory, song_id)
    if not os.path.exists(output_song_path):
        os.makedirs(output_song_path)



    input_song_path = os.path.join(songdata_directory, song_id)
    use_path(input_song_path)
    
    song_info = {
        "id": song_id,
        "charts": [],           # Difficulty IDs that this song has charts for (0 - Easy, 1 - Hard, 2 - Chaos, 3 - Glitch, 4 - Crash, 5 - Dream, 6 - Drop)
        "default_music": False, # Does the chart have a default music track?
        "musics": [],           # Difficulty IDs with custom music tracks (0 - Easy, 1 - Hard, 2 - Chaos, 3 - Glitch, 4 - Crash, 5 - Dream, 6 - Drop)
        "source": source_label
    }



    # Usually the filenames are exactly the same as the song ids, but there are a few exceptions
    song_filename = song_id
    exceptions = {
        "finalbossstage_1": "FinalBossStage_1",
        "finalbossstage_2": "FinalBossStage_2",
        "selectforpaffgameplay": "SelectForPaffGameplay",
        "ending05_staffroll": "Ending05_Staffroll"
    }
    if song_id in dict.keys(exceptions):
        song_filename = exceptions[song_id]



    # Search for charts
    chartdata_directory = traverse_and_use(input_song_path, ["game", "common", "bundleassets", "chartdata", song_id])
    for difficulty in range(0, 7):
        exists = try_copy(os.path.join(chartdata_directory, f"{song_filename}_{difficulty}.json"), os.path.join(output_song_path, f"chart_{difficulty}.json"))
        if exists:
            song_info["charts"].append(difficulty)

    # Search for musics
    musics_directory = traverse_and_use(input_song_path, ["game", "common", "bundleassets", "musics", song_id])
    song_info["default_music"] = try_copy(os.path.join(musics_directory, f"{song_filename}.ogg"), os.path.join(output_song_path, "song.ogg"))
    
    for difficulty in range(0, 7):
        exists = try_copy(os.path.join(musics_directory, f"{song_filename}_{difficulty}.ogg"), os.path.join(output_song_path, f"song_{difficulty}.ogg"))
        if exists:
            song_info["musics"].append(difficulty)


    # Load title, artist, and other info from extra_chart_metadata (chart_metadata.txt)
    if song_id in extra_chart_metadata:
        song_info["ordering_num"] = extra_chart_metadata[song_id]["ordering_num"]
        song_info["artist"] = extra_chart_metadata[song_id]["artist"]
        song_info["title"] = extra_chart_metadata[song_id]["title"]
        song_info["romanized_artist"] = extra_chart_metadata[song_id]["romanized_artist"]
        song_info["romanized_title"] = extra_chart_metadata[song_id]["romanized_title"]
        song_info["character"] = extra_chart_metadata[song_id]["character"]
        song_info["pack"] = extra_chart_metadata[song_id]["pack"]
        song_info["category"] = extra_chart_metadata[song_id]["category"]
        song_info["backupdb_filename"] = extra_chart_metadata[song_id]["backupdb_filename"]
        song_info["difficulties"] = extra_chart_metadata[song_id]["difficulties"]

    # Save song_info to file
    with open(os.path.join(output_song_path, "info.json"), "w") as info_file:
        json.dump(song_info, info_file, indent=4)


    
# Save extra_info to file
with open(os.path.join(output_directory, "extra_info.json"), "w") as extra_info_file:
    print("\nsaving extra_info.json...")
    json.dump(extra_info, extra_info_file, indent=4)

print("\033[32mdone\033[0m")
