import os
import sys
import json

import __metadata_reader

extra_chart_metadata = __metadata_reader.load_metadata(os.path.join(os.path.dirname(__file__), "chart_metadata.txt"))
if extra_chart_metadata is None:
    exit(1)



if len(sys.argv) < 2:
    print("\033[31merror: not enough arguments\033[0m", file=sys.stderr)
    print(f"\033[34musage: {sys.argv[0]} <input/output directory>\033[0m", file=sys.stderr)
    exit(1)

output_directory = sys.argv[1]



# Go through all songs and copy the chart and music files
for song_directory in os.scandir(output_directory):
    if not os.path.isdir(song_directory):
        continue

    song_id = song_directory.name
    print(f"\033[2K\033[1000D\033[37mprocessing {song_id}...\033[0m", end="", flush=True)
    
    with open(os.path.join(song_directory, "info.json"), "r") as info_file:
        song_info = json.load(info_file)

        # Load title, artist, and other info from extra_chart_metadata (chart_metadata.txt)
        if song_id in extra_chart_metadata:
            song_info["ordering_num"] = extra_chart_metadata[song_id]["ordering_num"]
            song_info["artist"] = extra_chart_metadata[song_id]["artist"]
            song_info["title"] = extra_chart_metadata[song_id]["title"]
            song_info["romanized_artist"] = extra_chart_metadata[song_id]["romanized_artist"]
            song_info["romanized_title"] = extra_chart_metadata[song_id]["romanized_title"]
            song_info["character"] = extra_chart_metadata[song_id]["character"]
            song_info["pack"] = extra_chart_metadata[song_id]["pack"]
            song_info["category"] = extra_chart_metadata[song_id]["category"]
            song_info["backupdb_filename"] = extra_chart_metadata[song_id]["backupdb_filename"]
            song_info["difficulties"] = extra_chart_metadata[song_id]["difficulties"]

        # Save song_info to file
        with open(os.path.join(song_directory, "info.json"), "w") as info_file:
            json.dump(song_info, info_file, indent=4)