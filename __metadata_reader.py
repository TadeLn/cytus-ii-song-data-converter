# Convert a string to an int if possible
def try_int(string):
    try:
        return int(string)
    except:
        return string

# Convert elements of an array to ints if possible
# Example: ["24", "3", "", "a"] => [24, 3, None, "a"]
def parse_diffs(array):
    for i in range(len(array)):
        if array[i] == "":
            array[i] = None
        else:
            array[i] = try_int(array[i])
    return array



# Load extra chart metadata
def load_metadata(path):
    extra_chart_metadata = {}
    try:
        with open(path, "r") as f:
            # The first line should be ignored
            lines = f.readlines()[1:]
            try:
                for line in lines:
                    # The format should be: ordering_num, artist, title, id, romanized_artist, romanized_title character, pack, category, easy, hard, chaos, glitch, crash, dream, drop
                    segments = line.split('\n')[0].split('\t')
                    id = segments[3]
                    extra_chart_metadata[id] = {
                        "ordering_num": try_int(segments[0]),
                        "artist": segments[1],
                        "title": segments[2],
                        "romanized_artist": segments[4],
                        "romanized_title": segments[5],
                        "character": segments[6],
                        "pack": segments[7],
                        "category": segments[8],
                        "backupdb_filename": segments[9],
                        "difficulties": parse_diffs(segments[10:])
                    }

            except Exception as ex:
                print("An error occurred: " + ex)
                pass
    except Exception as ex:
        print("Couldn't read extra chart metadata: " + ex)
        return None

    return extra_chart_metadata
